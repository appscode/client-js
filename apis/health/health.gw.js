// Code generated by protoc-gen-grpc-js-client
// source: health.proto
// DO NOT EDIT!

/*
This is a RSVP based Ajax client for gRPC gateway JSON APIs.
*/

var xhr = require('grpc-xhr');

function healthStatus(p, conf) {
    path = '/health/json'
    return xhr(path, 'GET', conf, p);
}

var services = {
    health: {
        status: healthStatus
    }
};

module.exports = {appscode: {health: services}};
