// Code generated by protoc-gen-grpc-js-client
// source: operation.proto
// DO NOT EDIT!

/*
This is a RSVP based Ajax client for gRPC gateway JSON APIs.
*/

var xhr = require('grpc-xhr');

function operationsDescribe(p, conf) {
    path = '/operation/v1alpha1/operations/' + p['phid'] + '/json'
    delete p['phid']
    return xhr(path, 'GET', conf, p);
}

function operationsDescribeLog(p, conf) {
    path = '/operation/v1alpha1/operations/' + p['phid'] + '/logs/' + p['log_id'] + '/json'
    delete p['phid']
    delete p['log_id']
    return xhr(path, 'GET', conf, p);
}

var services = {
    operations: {
        describe: operationsDescribe,
        describeLog: operationsDescribeLog
    }
};

module.exports = {appscode: {operation: {v1alpha1: services}}};
