var RSVP = require('rsvp');
var xhr = require('grpc-xhr');
var preset = require('grpc-xhr/config');
var auth = require('../apis/auth/v1alpha1/authentication.gw');

function ensureCsrfToken(req) {
  var now = Math.floor(Date.now() / 1000);
  return new RSVP.Promise(function (resolve, reject) {
    if (preset.headers['X-Phabricator-Csrf'] && preset.custom.tokenSetAt) {
      var elapsed = now - preset.custom.tokenSetAt;
      if (elapsed < 330 * 60 ) { // 5.5 hrs
        resolve(req || {});
      }
    }
    auth.appscode.auth.v1alpha1.authentication.cSRFToken()
      .then(function (resp) {
        preset.headers['X-Phabricator-Csrf'] = resp.csrf_token;
        preset.custom = {};
        preset.custom.tokenSetAt = now;
        resolve(req || {});
      })
      .catch(function (err) {
        if (window) {
          window.location.replace('/');
        } else {
          reject(err);
        }
      });
  });
};

module.exports = {
  ensureCsrfToken: ensureCsrfToken
};
